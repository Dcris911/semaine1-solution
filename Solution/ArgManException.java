
@SuppressWarnings("serial")
public class ArgManException extends Exception {

    public ArgManException(String errorMessage) {
        super(errorMessage);
    }
}
