
// "ArgMan" n'est qu'une interface, dont votre implémentation devra hériter.
// Au besoin, si vous le pensez nécessaire, vous pouvez y apporter des modifications.
public interface ArgMan {

    public void loadArgs(String[] args) throws ArgManException;

    public int getInt(char paramName) throws ArgManException;
    
    public boolean getBoolean(char paramName) throws ArgManException;
    
    public String getString(char paramName) throws ArgManException;
}
