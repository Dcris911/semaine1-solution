package divers;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.util.Lists;
import org.junit.Test;

public class DiversTest {

    @Test
    public void testAssertJ() {
        List<String> list = Lists.newArrayList("A", "B", "C");
        assertThat(list).contains("A"); // true
    }

    @Test
    public void testSoftAsserts() {
        List<String> stringList = Lists.newArrayList("A", "B", "C");
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(stringList).contains("A"); // true
        //softly.assertThat(stringList).containsOnly("A"); // false
        //softly.assertThat(stringList).containsExactly("A", "C", "B"); // false
        softly.assertThat(stringList).containsExactly("A", "B", "C"); // true
        // Don't forget to call SoftAssertions global verification!
        softly.assertAll();
    }

}
